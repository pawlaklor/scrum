<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tasks', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('type_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('assigned_user_id')->unsigned();
            $table->integer('project')->unsigned();
            $table->string('description');
            $table->string('name', 70);
            $table->timestamps();
            $table->timestamp('userscreated_at');

            $table->foreign('type_id')->references('id')->on('type_of_task');
            $table->foreign('status_id')->references('id')->on('status_of_task');
            $table->foreign('assigned_user_id')->references('id')->on('users');
            $table->foreign('project')->references('id')->on('projects');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('tasks');
	}

}

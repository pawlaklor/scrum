<?php

use App\Users\Role;
use App\Users\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

    public function run()
    {
        $u = new User();
        $u->password = "secret";
        $u->name = "Paweł";
        $u->surname = "Lorenc";
        $u->type = 1;
        $u->email = "pawlakzn@gmail.com";
        $u->telephone = 123456789;
        $u->save();

    }

}
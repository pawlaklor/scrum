<?php namespace App\Events\Users;

use App\Events\Event;

use App\User;
use Illuminate\Queue\SerializesModels;

class UserWasUpdated extends Event {

    use SerializesModels;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}

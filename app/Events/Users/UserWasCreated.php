<?php namespace App\Events\Users;

use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;

class UserWasCreated extends Event {

	use SerializesModels;

    private $user;
    private $password;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param $password
     */
	public function __construct(User $user, $password)
	{
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

}

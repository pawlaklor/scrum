<?php namespace App\Http\Controllers;

use App\Commands\Users\RemoveUserCommand;
use App\Commands\Users\CreateUserCommand;
use App\Commands\Users\UpdateUserCommand;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\User;
use Szykra\Notifications\Flash;

class UsersController extends Controller {

    /**
     * @var UserRetriever
     */
    private $users;

    /**
     * @param User $users
     */
   /* public function __construct(User $user)
    {
        $this->user = $user;
    }*/


	          /**
     * Display a listing of the resource.
     *
     * @return Response
     */
	public function index()
	{
       // $users = $this->users->paginate();

		//return view('users.index', compact('users'));
		return view('users.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('users.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUserRequest $request
     * @return Response
     */
	public function store(CreateUserRequest $request)
	{
        $user = $this->dispatchFrom(CreateUserCommand::class, $request);

        Flash::success("All done!", "User <strong>{$user->name} {$user->surname}</strong> has been created successfully!");

        return redirect()->route('users.index');
	}

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
	public function show($id)
	{
		$user = User::findOrFail($id);

        return view('users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $user = User::findOrFail($id);

        return view('users.edit', compact('user'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param  int $id
     * @return Response
     */
	public function update(UpdateUserRequest $request, $id)
	{
        $user = $this->dispatchFrom(UpdateUserCommand::class, $request);

        Flash::success("All done!", "User <strong>{$user->name} {$user->surname}</strong> has been updated successfully!");

        return redirect()->route('users.show', $id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $this->dispatch(new RemoveUserCommand($id));

        Flash::success("All done!", "User has been removed successfully!");

        return redirect()->route('users.index');
	}

}

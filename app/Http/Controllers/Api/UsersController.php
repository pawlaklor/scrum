<?php namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use HTML;
use Illuminate\Support\Facades\DB;
use yajra\Datatables\Datatables;

class UsersController extends Controller {

    public function getAllUsers()
	{
        $users = User::select([
            'id',
            'surname',
            'name',
            'email',
            'telephone',
            'created_at',
            'updated_at'
        ]);

        $data = Datatables::of($users)
            ->addColumn("actions", function($data) {
                return "<a href='". route('users.edit', [$data->id]) ."' class='btn btn-default btn-xs' title='Edit'><i class='fa fa-fw fa-pencil'></i></a>";
            })
            ->editColumn('email', function($data) {
                return HTML::mailto($data->email);
            })
            ->make();

        return $data;
	}

}

<?php

Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('<i class="fa fa-fw fa-home"></i> Dashboard', route('home'));
});

require_once __DIR__."/Breadcrumbs/users.php";

<?php

Breadcrumbs::register('users.index', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Users', route('users.index'));
});

Breadcrumbs::register('users.show', function($breadcrumbs, $user) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push($user->surname, route('users.show', $user->id));
});

Breadcrumbs::register('users.edit', function($breadcrumbs, $user) {
    $breadcrumbs->parent('users.show', $user);
    $breadcrumbs->push('Edit user', route('users.edit', $user->id));
});

Breadcrumbs::register('users.create', function($breadcrumbs) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('Add new user', route('users.create'));
});

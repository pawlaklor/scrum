<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

class CreateUserRequest extends Request {

    public function authorize()
    {
        return true; // TO CHANGE!
    }

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            'email'                 => 'required|unique:users|email',
            'name'            		=> 'required|max:100',
            'surname'             	=> 'required|max:100'
        ];
	}


}

<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

class UpdateUserRequest extends Request {

    public function authorize()
    {
        return true; // TO CHANGE!
    }

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $userId = $this->route()->getParameter('id');

        return [
            'id'                    => "required|in:{$userId}",
            'email'                 => "required|unique:users,email,{$userId}|email",
            'name'                  => 'required|max:100',
            'surname'               => 'required|max:100',
            'password_confirmation' => 'required_with:password|same:password',
        ];
	}

}

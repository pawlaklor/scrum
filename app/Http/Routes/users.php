<?php

/*$router->post('/api/users/search', [
    'uses' => 'UsersController@search'
]);*/

$router->get('/users', [
    'as' => 'users.index',
    'uses' => 'UsersController@index'
]);

$router->get('/users/create', [
    'as' => 'users.create',
    'uses' => 'UsersController@create'
]);

$router->get('/users/{id}', [
    'as' => 'users.show',
    'uses' => 'UsersController@show'
]);

$router->post('/users', [
    'as' => 'users.store',
    'uses' => 'UsersController@store'
]);

$router->get('/users/{id}/edit', [
    'as' => 'users.edit',
    'uses' => 'UsersController@edit'
]);

$router->put('/users/{id}', [
    'as' => 'users.update',
    'uses' => 'UsersController@update'
]);

$router->delete('/users/{id}', [
    'as' => 'users.destroy',
    'uses' => 'UsersController@destroy'
]);

<?php namespace App\Commands\Users;

use App\Commands\Command;

use App\User;
use App\Events\Users\UserWasUpdated;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Events\Dispatcher;

class UpdateUserCommand extends Command implements SelfHandling {

    private $id;
    private $email;
    private $password;
    private $name;
    private $surname;
    private $telephone;
    private $avatar;


    /**
     * Create a new command instance
     *
     * @param $id
     * @param $email
     * @param $password
     * @param $name
     * @param $surname
     * @param $telephone
     * @param $avatar
     */
    public function __construct($id, $email, $password, $name, $surname, $telephone, $avatar)
	{
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->telephone = $telephone;
        $this->avatar = $avatar;
    }

    /**
     * Execute the command.
     *
     * @param Dispatcher $dispatcher
     * @return \App\User
     */
	public function handle(Dispatcher $dispatcher)
	{
		$user = User::findOrFail($this->id);

        $user->email = $this->email;
        $user->password = $this->password;
        $user->name = $this->name;
        $user->surname = $this->surname;
        $user->telephone = $this->$telephone;
        $user->avatar = $this->$avatar;

        $user->save();

        $dispatcher->fire(new UserWasUpdated($user));

        return $user;
	}

}

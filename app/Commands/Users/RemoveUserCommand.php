<?php namespace App\Commands\Users;

use App\Commands\Command;
use App\Events\Users\UserWasRemoved;
use App\User;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Events\Dispatcher;

class RemoveUserCommand extends Command implements SelfHandling {

    /**
     * @var
     */
    private $userId;

    /**
     * Create a new command instance.
     *
     * @param $userId
     */
	public function __construct($userId)
	{
        $this->userId = $userId;
    }

    /**
     * Execute the command.
     *
     * @param Dispatcher $dispatcher
     */
	public function handle(Dispatcher $dispatcher)
	{
		$user = User::findOrFail($this->userId);

        $user->delete();

        $dispatcher->fire(new UserWasRemoved($user));
	}

}

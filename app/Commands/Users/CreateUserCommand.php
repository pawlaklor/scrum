<?php namespace App\Commands\Users;

use App\Commands\Command;
use App\Events\Users\UserWasCreated;
use App\User;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Events\Dispatcher;

class CreateUserCommand extends Command implements SelfHandling {

    private $email;
    private $name;
    private $surname;
    private $telephone;
    private $avatar;

    /**
     *  Create a new command instance.
     *
     * @param $email
     * @param $name
     * @param $surname
     * @param $telephone
     * @param $avatar
     */
    public function __construct($email, $name, $surname, $telephone, $avatar)
    {
        $this->email = $email;
        $this->name = $name;
        $this->surname = $surname;
        $this->telephone = $telephone;
        $this->avatar = $avatar;
    }

    /**
     * Execute the command.
     *
     * @param Dispatcher $dispatcher
     * @return User
     */
    public function handle(Dispatcher $dispatcher)
    {
        $password = $this->generatePassword();

        $user = new User([
            'password'   => $password,
            'email'      => $this->email,
            'name'       => $this->name,
            'surname'    => $this->surname,
            'telephone'  => $this->telephone,
            'avatar'     => $this->avatar
        ]);

        $user->save();

        $dispatcher->fire(new UserWasCreated($user, $password));

        return $user;
    }

    private function generatePassword($length = 13)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";

        return substr(str_shuffle($chars), 0, $length);
    }

}

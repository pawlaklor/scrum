<?php namespace App\Providers;

use Form;
use HTML;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerFormMacros();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    private function registerFormMacros()
    {
        Form::macro('error', function($field) {
            $errors = Session::get('errors');

            if($errors && $errors->has($field)) {
                return '<div class="text-danger small">'.$errors->first($field).'</div>';
            }

            return false;
        });

        Form::macro('group', function($field, Callable $content) {
            $html = [];
            $errors = Session::get('errors');

            if($errors && $errors->has($field)) {
                array_push($html, '<div class="form-group has-error">');
            }
            else {
                array_push($html, '<div class="form-group">');
            }

            array_push($html, $content());
            array_push($html, '</div>');

            return implode('', $html);

        });

        Form::macro('openGroup', function ($field) {
            $errors = Session::get('errors');

            if ($errors && $errors->has($field)) {
                return '<div class="form-group has-error">';
            }

            return '<div class="form-group">';
        });

        Form::macro('closeGroup', function() {
            return '</div>';
        });
    }

}

window.$ = window.jQuery = require('jquery');
require('bootstrap');

jQuery.noConflict();
;(function($) {
    'use strict';

    function Site(settings) {
        this.windowLoaded = false;
    }
    
    Site.prototype = {
        constructor: Site,

        start: function() {
            var me = this;

            $(window).load(function() {
                me.windowLoaded = true;
            });

            $.fn.dataTable = require('datatables');
            require('./vendor/dataTables.bootstrap.js');
            this.attach();
        },

        attach: function() {
          //  this.cameleon();
            this.global_sidebar();
            this.table_users();

        },

        cameleon: function(){
            $('.logo').on('click', function(e) {
            });
        },

        global_sidebar: function(){
            var $sidebar =  $('.global-sidebar');

            $('.js-sidebar-toggle').click(function(e) {
                $sidebar.toggleClass('opened');

                e.preventDefault();
            });
        },

        table_users: function(){
            $('.js-table-users').dataTable({
                "processing": true,
                "serverSide": true,
                "ajax": window.location.origin + "/api/users",
                "order": [[ 1, "asc" ]],

                /*
                 * This is workaround - server return one more column, because
                 * Customer model appends fake full_name attribute - current
                 * implementation of Laravel Datatables doesn't remove it.
                 */
                "columnDefs": [
                    {
                        "targets": [ 7 ],
                        "visible": false,
                        "searchable": false
                    }
                ]
            });
        }

    };

    jQuery(document).ready(function($) {
        var site = new Site();
        site.start();
    });

})(jQuery);
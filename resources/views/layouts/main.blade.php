@extends('layouts.html')

@section('body')
    <section class="global-sidebar opened">
        <ul class="list-unstyled">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard fa-2x fa-fw"></i> <span class="sidebar-icon-title">Dashboard</span></a></li>
            <li><a href="{{ url('/') }}"><i class="fa fa-file-text-o fa-2x fa-fw"></i> <span class="sidebar-icon-title">Projects</span></a></li>
            <li><a href="{{ url('/') }}"><i class="fa fa-user fa-2x fa-fw"></i> <span class="sidebar-icon-title">My profile</span></a></li>
            <li><a href="{{ url('/users') }}"><i class="fa fa-users fa-2x fa-fw"></i> <span class="sidebar-icon-title">Users</span></a> </li>
        </ul>
    </section>

    <div class="container-fluid main-container">
        @yield('content')
    </div>

    @include('layouts.header')

@endsection
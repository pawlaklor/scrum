<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="{{ $meta or '' }}">
    <meta name="keywords" content="{{ $keywords or '' }}">
	 <title>{{ $title or 'Scrum' }}</title>

	<link href="{{ asset('/assets/styles/main.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

</head>
<body>
	@yield('body')

	<!-- Scripts -->
	<script src="{{ asset('/assets/scripts/scripts.js') }}"></script>
</body>
</html>

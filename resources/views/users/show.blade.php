@extends('layouts.main')

{{--@section('breadcrumbs')
    {!! Breadcrumbs::render('admin.users.show', $user) !!}
@endsection--}}

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <h1 class="page-header"><i class="fa fa-user"></i> {{ $user->name }} {{ $user->surname }}</h1>

            <section class="panel panel-default">
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Name:</dt>
                        <dd>{{ $user->name }}</dd>

                        <dt>Last name:</dt>
                        <dd>{{ $user->surname }}</dd>

                        <dt>Email address:</dt>
                        <dd>{!! HTML::mailto($user->email) !!}</dd>

                        <dt>Telephone:</dt>
                        <dd>{{ $user->telephone }}</dd>

                        <dt>Created at:</dt>
                        <dd>{{ $user->created_at }}</dd>

                        <dt>Last update:</dt>
                        <dd><abbr title="{{ $user->updated_at }}">{{ $user->updated_at->diffForHumans() }}</abbr></dd>
                    </dl>
                </div>
            </section>
        </div>
    </div>
@endsection
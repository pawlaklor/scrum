@extends('layouts.main')

{{--
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin.users.create') !!}
@endsection
--}}

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <h1 class="page-header"><i class="fa fa-user-plus"></i> Add new user</h1>
            {!! Form::open(['route' => 'users.store', 'class' => 'form-horizontal']) !!}

            {!! Form::openGroup('email') !!}
            {!! Form::label('email', null, ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email address', 'required', 'maxlength' => 100]) !!}
                {!! Form::error('email') !!}
            </div>
            {!! Form::closeGroup() !!}

            {!! Form::openGroup('name') !!}
            {!! Form::label('name', null, ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required', 'maxlength' => 100]) !!}
                {!! Form::error('name') !!}
            </div>
            {!! Form::closeGroup() !!}

            {!! Form::openGroup('surname') !!}
            {!! Form::label('surname', null, ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('surname', null, ['class' => 'form-control', 'placeholder' => 'Surname', 'required', 'maxlength' => 100]) !!}
                {!! Form::error('surname') !!}
            </div>
            {!! Form::closeGroup() !!}

            {!! Form::openGroup('telephone') !!}
            {!! Form::label('telephone', null, ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('telephone', null, ['class' => 'form-control', 'placeholder' => 'Telephone']) !!}
                {!! Form::error('telephone') !!}
            </div>
            {!! Form::closeGroup() !!}

            {!! Form::openGroup('avatar') !!}
            {!! Form::label('avatar', null, ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('avatar', null, ['class' => 'form-control', 'placeholder' => 'Avatar']) !!}
                {!! Form::error('avatar') !!}
            </div>
            {!! Form::closeGroup() !!}

            <div class="text-right form-group">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-lg btn-primary">Create</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
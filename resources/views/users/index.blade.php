@extends('layouts.main')

{{--@section('breadcrumbs')
    {!! Breadcrumbs::render() !!}
@endsection--}}

@section('content')
    <section class="text-right">
        <a class="btn btn-primary" href="{{ route('users.create') }}"><i class="fa fa-fw fa-user-plus"></i> Add user</a>
    </section>

    <hr/>

    <table class="table table-striped table-bordered js-table-users">
        <thead>
        <tr>
            <th>#</th>
            <th>Surname</th>
            <th>Name</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th>Actions</th>
        </tr>
        </thead>
    </table>



   {{-- <section class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Surname</th>
                <th>Name</th>
                <th>Email</th>
                <th>Telephone</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td><a href="{{ route('users.show', $user->id) }}">{{ $user->surname }}</a></td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->telephone }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->updated_at }}</td>
                    <td>
                        <a href="{{ route('users.show', $user->id) }}" class="btn btn-xs btn-default" data-toggle="tooltip" title="Summary"><i class="fa fa-info fa-fw"></i></a>
                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil fa-fw"></i></a>

                            {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE', 'class' => 'inline form-destroy-'.$user->id]) !!}
                            <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" data-target=".form-destroy-{{ $user->id }}" data-confirm="remove" title="Remove"><i class="fa fa-remove fa-fw"></i></button>
                            {!! Form::close() !!}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>

    <section class="text-center">
        {!! $users->render() !!}
    </section>--}}

@endsection
var gulp = require('gulp'),
    browserfiy = require('gulp-browserify'),
    autoprefixer = require('gulp-autoprefixer'),
    less = require('gulp-less'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    debowerify = require('debowerify'),
    livereload = require('gulp-livereload'),
    gulpif = require('gulp-if');

var config = {

 // If you do not have the live reload extension installed,
 // set this to true. We will include the script for you,
 // just to aid with development.
 appendLiveReload: false,

 // Should CSS & JS be compressed?
 minifyCss: false,
 uglifyJS: false,

}

// CSS
gulp.task('css', function () {
 var stream = gulp
     .src('resources/assets/less/main.less')
     .pipe(less()).on('error', notify.onError(function (error) {
      return 'Error compiling LESS: ' + error.message;
     }))

     .pipe(autoprefixer())
     .pipe(gulpif(config.minifyCss, minifycss()))

     .pipe(gulp.dest('public/assets/styles'))
     .pipe(notify({message: 'Successfully compiled LESS'}));
});

// JS
gulp.task('js', function () {
 var scripts = [
  'bower_components/jquery/dist/jquery.js',

  'bower_components/bootstrap/js/transition.js',
  'bower_components/bootstrap/js/collapse.js',
  'bower_components/bootstrap/js/carousel.js',
  'bower_components/bootstrap/js/dropdown.js',
  'bower_components/bootstrap/js/tab.js',
  'bower_components/bootstrap/js/modal.js',

  'bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',

  'resources/assets/scripts/scripts.js'
 ]

 if (config.appendLiveReload === true) {
 // scripts.push('assets/js/livereload.js');
 }

 var stream = gulp
    // .src(scripts)
     .src('resources/assets/scripts/scripts.js')
     .pipe(concat('scripts.js'))
     .pipe(browserfiy({
         transform: ['debowerify'],
         insertGlobals: false,
         debug: false
     }))
     .pipe(gulpif(config.uglifyJS, uglify().on('error', notify.onError(function (error) {
      return 'Error compiling JS: ' + error.message;
     }))))

     .pipe(gulp.dest('public/assets/scripts'))
     .pipe(notify({message: 'Successfully compiled JavaScript'}))
});

// Default task
gulp.task('default', ['watch']);

// Watch
gulp.task('watch', function () {
    livereload.listen();

    // Watch .less files
    gulp.watch('resources/assets/less/**/*.less', ['css']).on('change', livereload.changed);

    // Watch .js files
    gulp.watch('resources/assets/scripts/**/*.js', ['js']).on('change', livereload.changed);

    gulp.watch('resources/views/**/*.php').on('change', livereload.changed);

});